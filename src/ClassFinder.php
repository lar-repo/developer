<?php

namespace Lar\Developer;

use File;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Traits\Macroable;
use ReflectionClass;
use Str;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Throwable;

/**
 * Class ClassFinder.
 * @package Lar\Developer
 */
class ClassFinder implements Arrayable
{
    use Macroable;

    /**
     * @var array
     */
    protected $files = [];

    /**
     * @var array
     */
    protected $dirs = [];

    /**
     * @var array
     */
    protected $result;

    /**
     * ClassFinder constructor.
     */
    public function __construct()
    {
        $this->getDirectories()->getAllClassFiles();
    }

    /**
     * @return $this
     */
    protected function getAllClassFiles()
    {
        foreach ($this->dirs as $dir) {
            $files = collect($this->files($dir))
                ->map(function (SplFileInfo $file) {
                    return $file->getPathname();
                })
                ->filter(function (string $file) {
                    return Str::is('*.php', $file) && is_file($file);
                })
                ->map('class_in_file')->filter();

            $this->files = array_merge($this->files, $files->toArray());
        }

        return $this;
    }

    /**
     * @param $dir
     * @return array
     */
    protected function files($dir)
    {
        return iterator_to_array(
            Finder::create()->files()->ignoreDotFiles(true)
                ->name('*.php')->in($dir)->sortByName(),
            false
        );
    }

    /**
     * @return $this
     */
    protected function getDirectories()
    {
        foreach (File::directories(base_path()) as $directory) {
            if (
                !Str::is('*vendor', $directory) &&
                !Str::is('*node_modules', $directory) &&
                !Str::is('*public', $directory)
            ) {
                $this->dirs[] = $directory;
            }
        }

        return $this;
    }

    /**
     * @return static
     */
    public static function create()
    {
        return new static();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->get();
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->files;
    }

    /**
     * Classes where has current interface.
     * @param  string  $subject
     * @return $this
     */
    public function by_interface(string $subject)
    {
        return $this->filter(function (ReflectionClass $class, $class_name) use ($subject) {
            return $class->implementsInterface($subject);
        });
    }

    /**
     * @param  callable  $check
     * @return ClassFinder
     */
    protected function filter(callable $check)
    {
        $result = [];

        foreach ($this->files as $file) {
            try {
                $ref = new ReflectionClass($file);
            } catch (Throwable $exception) {
                continue;
            }

            if (call_user_func($check, $ref, $file)) {
                $result[] = $file;
            }
        }

        $this->files = $result;

        return $this;
    }

    /**
     * Classes where has current parent.
     * @param  string  $subject
     * @return $this
     */
    public function by_class(string $subject)
    {
        return $this->filter(function (ReflectionClass $class, $class_name) use ($subject) {
            return is_a($class_name, $subject, true);
        });
    }

    /**
     * Classes where has current constant.
     * @param  string  $subject
     * @return $this
     */
    public function has_constant(string $subject)
    {
        return $this->filter(function (ReflectionClass $class, $class_name) use ($subject) {
            return $class->hasConstant($subject);
        });
    }

    /**
     * Classes where has current method.
     * @param  string  $subject
     * @return $this
     */
    public function has_method(string $subject)
    {
        return $this->filter(function (ReflectionClass $class, $class_name) use ($subject) {
            return $class->hasMethod($subject);
        });
    }

    /**
     * Classes where has current property.
     * @param  string  $subject
     * @return $this
     */
    public function has_property(string $subject)
    {
        return $this->filter(function (ReflectionClass $class, $class_name) use ($subject) {
            return $class->hasProperty($subject);
        });
    }

    /**
     * Classes where subclass of.
     * @param  mixed  $subject
     * @return $this
     */
    public function is_subclass_of($subject)
    {
        return $this->filter(function (ReflectionClass $class, $class_name) use ($subject) {
            return $class->isSubclassOf($subject);
        });
    }

    /**
     * Classes where namespace start with current.
     * @param  mixed  $subject
     * @return $this
     */
    public function is_start_with(string $subject)
    {
        return $this->filter(function (ReflectionClass $class, $class_name) use ($subject) {
            return Str::is('\\'.trim($subject).'*', $class_name);
        });
    }

    /**
     * Classes where namespace end with current.
     * @param  mixed  $subject
     * @return $this
     */
    public function is_end_with(string $subject)
    {
        return $this->filter(function (ReflectionClass $class, $class_name) use ($subject) {
            return Str::is('*'.trim($subject), $class_name);
        });
    }

    /**
     * Classes where class name is current.
     * @param  mixed  $subject
     * @return $this
     */
    public function is_name(string $subject)
    {
        return $this->filter(function (ReflectionClass $class, $class_name) use ($subject) {
            return class_basename($class_name) == class_basename($subject);
        });
    }

    /**
     * Classes in namespace.
     * @return $this
     */
    public function in_namespace()
    {
        return $this->filter(function (ReflectionClass $class, $class_name) {
            return $class->inNamespace();
        });
    }

    /**
     * Abstract classes.
     * @return $this
     */
    public function is_abstract()
    {
        return $this->filter(function (ReflectionClass $class, $class_name) {
            return $class->isAbstract();
        });
    }

    /**
     * Anonymous classes.
     * @return $this
     */
    public function is_anonymous()
    {
        return $this->filter(function (ReflectionClass $class, $class_name) {
            return $class->isAnonymous();
        });
    }

    /**
     * Cloneable classes.
     * @return $this
     */
    public function is_cloneable()
    {
        return $this->filter(function (ReflectionClass $class, $class_name) {
            return $class->isCloneable();
        });
    }

    /**
     * Final classes.
     * @return $this
     */
    public function is_final()
    {
        return $this->filter(function (ReflectionClass $class, $class_name) {
            return $class->isFinal();
        });
    }

    /**
     * Instantiable classes.
     * @return $this
     */
    public function is_instantiable()
    {
        return $this->filter(function (ReflectionClass $class, $class_name) {
            return $class->isInstantiable();
        });
    }

    /**
     * Interface classes.
     * @return $this
     */
    public function is_interface()
    {
        return $this->filter(function (ReflectionClass $class, $class_name) {
            return $class->isInterface();
        });
    }

    /**
     * Iterable classes.
     * @return $this
     */
    public function is_iterable()
    {
        return $this->filter(function (ReflectionClass $class, $class_name) {
            return $class->isIterable();
        });
    }
}
