<?php

namespace Lar\Developer\Commands;

use Illuminate\Console\Command;

/**
 * Class SuperVisorCfgGenerator.
 *
 * @package Lar\Developer\Commands
 */
class SuperVisorCfgGenerator extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'lar:make-supervisor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Supervisor config';
    /**
     * @var bool
     */
    protected $begin = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->comment("\n\tWelcome to <info>supervisor</info> Config generator!\n");

        $name = $this->ask('Config name:', env('APP_NAME'));

        $process_name = '%(program_name)s_%(process_num)02d';

        $numprocs = 3;

        $multi = false;

        if ($multi = $this->confirm('Create customization for multiple processes?', $multi)) {
            $process_name = $this->ask('Process name:', $process_name);

            $numprocs = $this->ask('The number of processes:', $numprocs);
        }

        $command = $this->ask('Command:', 'php '.base_path('artisan queue:work --tries=1'));

        $autostart = true; //$this->confirm("Enable auto start?", true);

        $autorestart = true; //$this->confirm("Enable auto restart?", true);

        $redirect_stderr = true; //$this->confirm("Enable redirect stderr?", true);

        $user = $this->ask('System Username:', exec('whoami'));

        $stdout_logfile = $this->ask('Log File:', base_path("worker-{$name}.log"));

        $cfg = "[program:{$name}]\n";

        $cfg .= (!$multi ? '#' : '')."process_name={$process_name}\n";

        $cfg .= "command={$command}\n";

        $cfg .= 'autostart='.($autostart ? 'true' : 'false')."\n";

        $cfg .= 'autorestart='.($autorestart ? 'true' : 'false')."\n";

        $cfg .= "user={$user}\n";

        $cfg .= (!$multi ? '#' : '')."numprocs={$numprocs}\n";

        $cfg .= 'redirect_stderr='.($redirect_stderr ? 'true' : 'false')."\n";

        $cfg .= "stdout_logfile={$stdout_logfile}";

        $file = storage_path("{$name}.conf");

        file_put_contents($file, $cfg);

        $this->info("Generate done! Saved to [{$file}]");
    }
}
