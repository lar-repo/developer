<?php

namespace Lar\Developer\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Lar\Developer\Commands\Dump\GenerateJaxHelper;
use Lar\EntityCarrier\Core\Entities\DocumentorEntity;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class SeedPullCommand.
 * @package Lar\Developer\Commands
 */
class SeedPullCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'seed:pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull model to seed class';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $models = $this->argument('model');

        foreach (explode(',', $models) as $model) {
            $model = trim($model);

            if (!class_exists($model)) {
                if (class_exists("App\\Models\\{$model}")) {
                    $model = "App\\Models\\{$model}";
                } elseif (class_exists("App\\{$model}")) {
                    $model = "App\\{$model}";
                } else {
                    $this->error("Model [{$model}] not found!");
                    continue;
                }
            }

            $name = explode('\\', $model);
            $model_name = $name[array_key_last($name)];
            $name = "{$model_name}Seeder";
            /** @var Model $model */
            $data = $model::all()->toArray();

            $class = class_entity($name);
            $class->wrap('php');
            $class->use($model);
            $class->prop('protected:data', $data);
            $class->extend(Seeder::class);
            $method = $class->method('run');
            $method->doc(function (DocumentorEntity $doc) {
                $doc->description('Run the database seeds.');
                $doc->tagReturn('void');
            });
            $method->line("{$model_name}::insert(\$this->data);");

            file_put_contents(database_path("seeds/{$name}.php"), $class->render());
            $this->info('Seed: ['.database_path("seeds/{$name}.php").'] Created!');
        }

        return 0;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['model', InputArgument::REQUIRED, 'Job model'],
        ];
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['all', 'a', InputOption::VALUE_NONE, 'Where was commit'],
            //['tag', 't', InputOption::VALUE_OPTIONAL, 'Set tag name (only for once use)'],
        ];
    }
}
