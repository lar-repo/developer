<?php

namespace Lar\Developer\Commands;

use Cache;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class LarGitTag extends Command
{
    /**
     * @var array
     */
    protected static $dirs = [];
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'git:tag';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic station to tagable lar packages';

    /**
     * @param  string  $dir
     */
    public static function addDir(string $dir)
    {
        static::$dirs[] = $dir;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fs = new Filesystem();

        $dirs = is_link(base_path('lar')) ? $fs->directories(base_path('lar')) : [];

        $dirs = array_merge($dirs, static::$dirs);

        $delete = $this->option('delete');
        $folder = $this->option('folder');

        if ($delete) {
            $path = base_path((string) $folder);
            $cd = "cd {$path} && ";
            exec("{$cd}git tag -d {$delete}");
            exec("{$cd}git push --delete origin {$delete}");

            return 0;
        }

        $i = 0;

        foreach ($dirs as $dir) {
            $name = Arr::last(explode('/', $dir));

            if (!$this->argument('package') && !$this->option('all') && !Cache::has("for_tag:{$dir}")) {
                continue;
            }

            if ($this->argument('package') && $this->argument('package') !== $name) {
                continue;
            }

            if (!is_dir($dir.'/.git')) {
                continue;
            }

            $has_commit = Cache::has("for_tag:{$dir}");

            if (!$this->argument('package') && !$this->confirm("Create tag for [{$name}]?", $has_commit)) {
                continue;
            }

            $cd = "cd {$dir} && ";

            $tag_list = [];

            exec("{$cd}git tag", $tag_list);

            sort($tag_list, SORT_NATURAL);

            $last_tag = '1.0.-1';
            $now_tag = 'master';

            $this->comment('Creating a new tag...');

            foreach ($tag_list as $item) {
                if (preg_match('/^([^\(][a-zA-Z0-9\_\-\:\.]+[^\)])$/', $item, $m)) {
                    $now_tag = $m[1];
                    $last_tag = $now_tag;
                }
            }

            $last_tag = ltrim($last_tag, 'v');
            $last_tag = explode('.', $last_tag);
            $last_tag[array_key_last($last_tag)] = (int) $last_tag[array_key_last($last_tag)] + 1;
            $last_tag = implode('.', $last_tag);

            if ($this->argument('package') && $t = $this->option('tag')) {
                $tag = $t == '+' ? $last_tag : $t;
            } else {
                $tag = $this->ask("Enter a new tag for [{$name}][{$now_tag}]", "v{$last_tag}");
            }

            $tag = 'v'.ltrim($tag, 'v');

            $message = $this->ask('Enter description of the tag', 'New version '.$tag);
            $this->info("Creating a new tag [{$tag}][{$message}]...");
            exec("{$cd}git tag -a {$tag} -m '{$message}'");
            exec("{$cd}git push --tag");

            $this->info("[{$name}] finished!");

            if ($has_commit) {
                Cache::forget("for_tag:{$dir}");
            }

            $i++;
        }

        if (!$i) {
            $this->error('Nothing a tagging!');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['package', InputArgument::OPTIONAL, 'The name of the package for tagging.'],
        ];
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['delete', 'd', InputOption::VALUE_OPTIONAL, 'Delete tag name'],
            ['folder', 'f', InputOption::VALUE_OPTIONAL, 'Folder for delete'],
            ['all', 'a', InputOption::VALUE_NONE, 'Where was commit'],
            ['tag', 't', InputOption::VALUE_OPTIONAL, 'Set tag name (only for once use)'],
        ];
    }
}
