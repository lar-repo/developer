<?php

namespace Lar\Developer\Commands;

use Cache;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;
use Symfony\Component\Console\Input\InputOption;

class LarGit extends Command
{
    /**
     * @var array
     */
    protected static $dirs = [];
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'git';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic station to create and unload commits.';
    /**
     * Global iterator.
     *
     * @var int
     */
    protected $i = 0;
    protected $tmp_files = [];

    /**
     * @param  string  $dir
     */
    public static function addDir(string $dir)
    {
        static::$dirs[] = $dir;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $auto = !$this->option('fool-steps');

        $show_status = $this->option('status');

        $info = $this->option('info');

        $fs = new Filesystem();

        $dirs = is_link(base_path('lar')) ? $fs->directories(base_path('lar')) : [];

        $dirs = array_merge($dirs, static::$dirs);

        if (!$this->option('lar')) {
            if ($this->option('base')) {
                $dirs = [base_path()];
            } else {
                $dirs[] = base_path();
            }
        }

        foreach ($dirs as $key => $dir) {
            $name = strtoupper(Arr::last(explode('/', $dir)));

            if (!is_dir($dir.'/.git')) {
                continue;
            }

            $cd = "cd {$dir} && ";

            $branch_exec_list = [];

            $branch = null;
            $remote = null;

            exec("{$cd}git branch", $branch_exec_list);

            foreach ($branch_exec_list as $item) {
                if (preg_match('/^\*\s([^\(][a-zA-Z0-9\_\-\:\.]+[^\)])$/', $item, $m)) {
                    $branch = $m[1];
                }
            }

            if (base_path() !== $dir) {
                $bbb = str_replace(base_path().'/', '', $dir);
            } else {
                $bbb = config('app.name');
            }

            $CB = "GIT[{$bbb}][".($branch ? $branch : $key).'] ';

            $this->comment("{$CB}status...");

            $status = [];

            exec("{$cd}git status", $status);

            if (count($status) > 5) {
                $files = [];
                $files_data = [];
                $this->i = 0;
                $this->tmp_files = [];

                foreach ($status as $stat) {
                    if (preg_match('/([a-zA-Z\-\_\.\\\\\/]+\.[a-zA-Z]{1,5})/', $stat, $m)) {
                        if ($info) {
                            $this->info($stat);
                        }

                        $files[$m[1]] = $m[1];

                        $mm = explode(':', $stat);

                        if (!isset($mm[1])) {
                            $mm[1] = $mm[0];
                            $mm[0] = 'undefined';
                        }

                        $files_data[trim($mm[0]).':'.trim($mm[1])] = trim($mm[1]);
                    } else {
                        if ($info) {
                            $this->comment($stat);
                        }
                    }
                }

                $rows = collect($files_data)->map(function ($item, $key) {
                    $key = explode(':', $key);

                    $i = $this->i++;

                    $this->tmp_files["F{$i}"] = $item;

                    return ['F'.$i, $key[0], $item];
                })->values()->toArray();

                if (!$info && count($files_data)) {
                    $this->table(['No', 'Status', 'Object'], $rows);
                }

                if (!$show_status) {
                    if ($auto || $this->confirm('Add all?', true)) {
                        $this->comment("{$CB}add...");

                        system("{$cd}git add .");

                        if ($auto || $this->confirm('Create commit?', 'Yes')) {
                            $this->comment("{$CB}commit...");

                            $comment = $this->ask('ENTER a comment from commit or empty from auto comment');

                            if (empty($comment)) {
                                $comment = 'Changed: ';

                                if (count($files)) {
                                    $comment .= implode(', ', $files);
                                }
                            } else {
                                $comment = ucfirst($comment);
                                $comment = str_replace([':list', ':files', ':file'], implode(', ', $files), $comment);
                                $comment = str_replace([':date', ':time', ':datetime'],
                                    [now()->toDateString(), now()->toTimeString(), now()->toDateTimeString()],
                                    $comment);
                                $comment = str_replace([':bb', ':branch'], $branch, $comment);
                                $comment = str_replace(array_keys($this->tmp_files), array_values($this->tmp_files),
                                    $comment);
                                $comment = str_replace([':List', ':Files', ':File'],
                                    implode(', ', collect($files)->map(function ($item) {
                                        return Arr::last(explode('/', $item));
                                    })->toArray()), $comment);
                            }

                            $this->comment("{$CB}COMMENT "."<info>[$comment]</info>");

                            $commit_comment_cli = [];

                            exec("{$cd}git commit -m '{$comment}'", $commit_comment_cli);

                            if ($info) {
                                foreach ($commit_comment_cli as $item) {
                                    $this->info("{$CB}{$item}");
                                }
                            }

                            if ($auto || $this->confirm('Push?', true)) {
                                $results = [];

                                if ($branch) {
                                    $this->comment("{$CB}push to branch [{$branch}]...");

                                    exec("{$cd}git push origin {$branch}", $results);
                                } else {
                                    $this->comment("{$CB}push...");

                                    exec("{$cd}git push", $results);
                                }

                                Cache::forever("for_tag:{$dir}", true);

                                if ($info) {
                                    foreach ($results as $result) {
                                        $this->info($result);
                                    }
                                }
                            }
                        }

                        $msg = "{$CB}WORKING DONE!";
                    }

                    $this->info($msg);
                } else {
                    $msg = "{$CB}HAVE CHANGES! <<<";

                    $this->info($msg);
                }
            } else {
                $msg = "{$CB}NO CHANGED!";

                $this->info($msg);
            }

            $this->line(str_repeat('#', strlen($msg)));
        }
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        return [
            [
                'fool-steps', 'f', InputOption::VALUE_NONE,
                'Off everything in automatic mode, with the ability to edit the comments of commits.'
            ],
            ['base', 'b', InputOption::VALUE_NONE, 'Only the base project directory.'],
            ['lar', 'l', InputOption::VALUE_NONE, 'Only the lar project directory.'],
            ['status', 's', InputOption::VALUE_NONE, 'Display only statuses.'],
            ['info', 'i', InputOption::VALUE_NONE, 'Display original status information.'],
        ];
    }
}
