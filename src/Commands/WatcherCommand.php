<?php

namespace Lar\Developer\Commands;

use Illuminate\Console\Command;
use Lar\Developer\Commands\Dump\GenerateBladeHelpers;
use Lar\Developer\Commands\Dump\GenerateHelper;
use Lar\Developer\Commands\Dump\GenerateJaxHelper;
use Lar\Developer\Commands\Dump\GenerateNewJaxHelper;
use Lar\Developer\Commands\Dump\GetterHelper;

class WatcherCommand extends Command
{
    /**
     * @var array
     */
    protected static $dirs = [];
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'watch';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic watcher for help';

    /**
     * @param  array  $rules
     */
    public static function rules(array $rules)
    {
        static::$dirs = array_merge_recursive(static::$dirs, $rules);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        static::dumpClass(app_path('Jax'), GenerateNewJaxHelper::class);
        static::dumpClass(app_path('Components'), GenerateHelper::class);
        static::dumpClass(app_path('Components'), GenerateBladeHelpers::class);
        static::dumpClass(app_path('Getters'), GetterHelper::class);
        static::addDir(app_path('Models'), 'php artisan ide-helper:models -W');

        $cd = 'cd '.__DIR__.'/../../watcher && ';
        system(
            "$cd node app.js ".
            base64_encode(json_encode(static::$dirs)).
            ' '.base_path()
        );
    }

    /**
     * @param  string  $dir
     * @param  string  $class
     */
    public static function dumpClass(string $dir, string $class)
    {
        static::addDir($dir, "php artisan lar:dump --class=\"\\$class\"");
    }

    /**
     * @param  string  $dir
     * @param  string|array  $command
     */
    public static function addDir(string $dir, $command)
    {
        if (is_string($command)) {
            static::$dirs[realpath($dir)][] = $command;
        } elseif (is_array($command)) {
            static::$dirs[realpath($dir)] = array_merge(
                static::$dirs[realpath($dir)] ?? [],
                $command
            );
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['package', InputArgument::OPTIONAL, 'The name of the package for tagging.'],
        ];
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['all', 'a', InputOption::VALUE_NONE, 'Where was commit'],
            //['tag', 't', InputOption::VALUE_OPTIONAL, 'Set tag name (only for once use)'],
        ];
    }
}
