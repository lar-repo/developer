<?php

namespace Lar\Developer\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class LarGitCheckout extends Command
{
    /**
     * @var array
     */
    protected static $dirs = [];
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'git:checkout';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic station to checkout lar packages';

    /**
     * @param  string  $dir
     */
    public static function addDir(string $dir)
    {
        static::$dirs[] = $dir;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $fs = new Filesystem();

        $dirs = is_link(base_path('lar')) ? $fs->directories(base_path('lar')) : [];

        $dirs = array_merge($dirs, static::$dirs);

        foreach ($dirs as $dir) {
            if (!is_dir($dir.'/.git')) {
                continue;
            }

            $cd = "cd {$dir} && ";

            exec("{$cd}git branch", $branch_exec_list);

            $last_branch = 'master';

            foreach ($branch_exec_list as $item) {
                if (preg_match('/^\*\s([^\(][a-zA-Z0-9\_\-\:\.]+[^\)])$/', $item, $m)) {
                    $last_branch = $m[1];
                }
            }

            $branch = $this->argument('branch') ?? $last_branch;

            $b_flag = ' ';

            if ($this->option('new-branch')) {
                $new_branch = $this->ask('What branch name for create?', $branch);

                if ($new_branch != $branch) {
                    $branch = $new_branch;
                    $b_flag = ' -b ';
                }
            }

            exec("{$cd}git checkout{$b_flag}{$branch}", $status);

            $status = is_array($status) ? (isset($status[array_key_last($status)]) ? $status[array_key_last($status)] : 'OK') : $status;

            $this->info($dir);
            $this->comment($status);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['branch', InputArgument::OPTIONAL, 'The name of the branch. (master) - by default'],
        ];
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['new-branch', 'b', InputOption::VALUE_NONE, 'Create and checkout a new branch'],
        ];
    }
}
