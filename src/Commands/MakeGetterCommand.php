<?php

namespace Lar\Developer\Commands;

use Arr;
use Illuminate\Console\Command;
use Lar\Developer\Getter;
use Lar\Layout\CfgFile;
use Str;

/**
 * Class DumpAutoload.
 *
 * @package Lar\Developer\Commands
 */
class MakeGetterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:getter { name : Name of getter}
    {--dir= : Dir in to created path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a getter';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dir = base_path($this->option('dir') ? $this->option('dir') : 'app/Getters');

        $namespace = 'App\\Getters';

        foreach ($this->component_segments() as $component_segment) {
            if ($component_segment != $this->component_name()) {
                $normal_part = ucfirst(Str::camel(Str::snake($component_segment)));

                $namespace .= "\\{$normal_part}";

                $dir .= "/{$normal_part}";
            }
        }

        $file = $dir.'/'.$this->class_name().'.php';

        $class_namespace = "{$namespace}\\{$this->class_name()}";

        if (!is_dir($dir)) {
            mkdir($dir, 0777, 1);
        }

        if (is_file($file)) {
            $this->error("The component [{$this->class_name()}] already exists!");

            return;
        }

        $entity = class_entity($this->class_name());
        $entity->wrap('php');
        $entity->namespace($namespace);
        $entity->extend(Getter::class);

        $entity->method('access')->line()->dataReturn('true')->docReturnType('bool');
        $entity->method('default')->line()->dataReturn('null')->docReturnType('mixed');

        if (file_put_contents($file, $entity->render())) {
            CfgFile::open(config_path('gets.php'))->write($this->name(), $class_namespace);

            $this->info('Config ['.$this->name().'] add to [config/gets.php]!');

            $this->info("Getter [{$file}] created!");
        }
    }

    /**
     * @return array
     */
    protected function component_segments()
    {
        return array_map('Str::snake', explode('/', $this->input->getArgument('name')));
    }

    /**
     * @return mixed
     */
    protected function component_name()
    {
        return Arr::last($this->component_segments());
    }

    /**
     * Get class name.
     *
     * @return string|string[]|null
     */
    protected function class_name()
    {
        return ucfirst(Str::camel($this->name()));
    }

    /**
     * @return string
     */
    protected function name()
    {
        return Str::slug($this->component_name(), '_');
    }
}
