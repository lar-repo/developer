<?php

namespace Lar\Developer\Commands;

use Illuminate\Console\Command;
use ReflectionException;

class ICode extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'lar:i-code {code*} {--json}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run LAR ICode';
    /**
     * @var bool
     */
    protected $begin = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws ReflectionException
     */
    public function handle()
    {
        $codes = $this->argument('code');

        $results = [];

        foreach ($codes as $code) {
            $result = trim(i_code('{'.trim($code, '{}').'}'), "{} \t\n\r \v");

            if (!empty($result)) {
                $results[] = $result;
            } else {
                $results[] = 'ERROR';
            }
        }

        if ($this->option('json')) {
            $this->line(json_encode($results));
        } else {
            foreach ($results as $result) {
                $this->line($result);
            }
        }
    }
}
