<?php

namespace Lar\Developer\Commands\Dump;

use Illuminate\Console\Command;

/**
 * Interface DumpExecute.
 *
 * @package Lar\Layout\Commands\Dump
 */
interface DumpExecute
{
    /**
     * Handle call method.
     *
     * @param  Command  $command
     * @return mixed
     */
    public function handle(Command $command);
}
