<?php

namespace Lar\Developer\Commands\Dump;

use Exception;
use Illuminate\Console\Command;
use Lar\Layout\CfgFile;
use Lar\Layout\Layout;
use Lar\Tagable\Tag;

class InjectorClear implements DumpExecute
{
    /**
     * @var Command
     */
    protected $command;

    /**
     * Handle call method.
     *
     * @param  Command  $command
     * @return mixed
     */
    public function handle(Command $command)
    {
        $this->command = $command;

        $this->clear_tag_injectors();

        $this->clear_layout_injectors();

        $this->jax_executors();

        $this->ws_executors();
    }

    /**
     * Clear TAG Injector files.
     */
    public function clear_tag_injectors()
    {
        Tag::$components->map(function ($item, $key) {
            if (!class_exists($item)) {
                $this->command->info("Warning!!!!!!!!!!! [{$key}][{$item}] not found!");
            }
        });

        foreach (Tag::$injected_files as $injected_file) {
            $file = new CfgFile($injected_file);

            foreach ($file->data() as $key => $item) {
                try {
                    if (!class_exists($item)) {
                        $file->remove($key);

                        $this->command->comment("<error>Remove component: </error>[{$key}][{$item}] from [{$injected_file}]");
                    }
                } catch (Exception $exception) {
                    if (preg_match('/(No\ssuch\sfile\sor\sdirectory)/', $exception->getMessage())) {
                        $file->remove($key);

                        $this->command->comment("<error>Remove JAX executor: </error>[{$key}][{$item}] from [config/executors.php]");
                    }
                }
            }
        }

        if (is_file($injected_file = config_path('components.php'))) {
            $file = new CfgFile($injected_file);

            foreach ($file->data() as $key => $item) {
                try {
                    if (!class_exists($item)) {
                        $file->remove($key);

                        $this->command->comment("<error>Remove component: </error>[{$key}][{$item}] from [config/components.php]");
                    }
                } catch (Exception $exception) {
                    if (preg_match('/(No\ssuch\sfile\sor\sdirectory)/', $exception->getMessage())) {
                        $file->remove($key);

                        $this->command->comment("<error>Remove JAX executor: </error>[{$key}][{$item}] from [config/executors.php]");
                    }
                }
            }
        }
    }

    /**
     * Clear Layout Injector files.
     */
    public function clear_layout_injectors()
    {
        foreach (Layout::$injected_files as $injected_file) {
            $file = new CfgFile($injected_file);

            foreach ($file->data() as $key => $item) {
                try {
                    if (!class_exists($item)) {
                        $file->remove($key);

                        $this->command->comment("<error>Remove layout component: </error>[{$key}][{$item}] from [{$injected_file}]");
                    }
                } catch (Exception $exception) {
                    if (preg_match('/(No\ssuch\sfile\sor\sdirectory)/', $exception->getMessage())) {
                        $file->remove($key);

                        $this->command->comment("<error>Remove JAX executor: </error>[{$key}][{$item}] from [config/executors.php]");
                    }
                }
            }
        }

        if (Layout::$collect) {
            Layout::$collect->map(function ($item, $key) {
                if (!class_exists($item)) {
                    $this->command->info("Warning!!!!!!!!!!! [Layout][{$key}][{$item}] not found!");
                }
            });
        }
    }

    public function jax_executors()
    {
        $file = new CfgFile(app()->bootstrapPath('jax_executors.php'));

        foreach ($file->data() as $key => $item) {
            try {
                if (!class_exists($item)) {
                    $file->remove($key);

                    $this->command->comment("<error>Remove JAX executor: </error>[{$key}][{$item}] from [config/jax_executors.php]");
                }
            } catch (Exception $exception) {
                if (preg_match('/(No\ssuch\sfile\sor\sdirectory)/', $exception->getMessage())) {
                    $file->remove($key);

                    $this->command->comment("<error>Remove JAX executor: </error>[{$key}][{$item}] from [config/executors.php]");
                }
            }
        }

        $file = new CfgFile(config_path('executors.php'));

        foreach ($file->data() as $key => $item) {
            try {
                if (!class_exists($item)) {
                    $file->remove($key);

                    $this->command->comment("<error>Remove JAX executor: </error>[{$key}][{$item}] from [config/executors.php]");
                }
            } catch (Exception $exception) {
                if (preg_match('/(No\ssuch\sfile\sor\sdirectory)/', $exception->getMessage())) {
                    $file->remove($key);

                    $this->command->comment("<error>Remove JAX executor: </error>[{$key}][{$item}] from [config/executors.php]");
                }
            }
        }
    }

    public function ws_executors()
    {
        $file = new CfgFile(app()->bootstrapPath('ws_executors.php'));

        foreach ($file->data() as $key => $item) {
            try {
                if (!class_exists($item)) {
                    $file->remove($key);

                    $this->command->comment("<error>Remove WS executor: </error>[{$key}][{$item}] from [ws_executors.php]");
                }
            } catch (Exception $exception) {
                if (preg_match('/(No\ssuch\sfile\sor\sdirectory)/', $exception->getMessage())) {
                    $file->remove($key);

                    $this->command->comment("<error>Remove JAX executor: </error>[{$key}][{$item}] from [config/executors.php]");
                }
            }
        }
    }
}
