<?php

namespace Lar\Developer\Commands\Dump;

use Illuminate\Console\Command;
use Lar\Developer\Getter;
use Lar\EntityCarrier\Core\Entities\ClassEntity;
use Lar\EntityCarrier\Core\Entities\DocumentorEntity;
use Lar\EntityCarrier\Core\Entities\NamespaceEntity;
use Lar\EntityCarrier\Core\Entities\ParamEntity;
use ReflectionClass;
use ReflectionException;
use Str;

/**
 * Class GetterHelper.
 * @package Lar\Developer\Commands\Dump
 */
class GetterHelper implements DumpExecute
{
    /**
     * @var Command
     */
    protected $command;

    /**
     * @var ClassEntity
     */
    protected $class;

    /**
     * Handle call method.
     *
     * @param  Command  $command
     * @return mixed
     */
    public function handle(Command $command)
    {
        $this->command = $command;

        $namespace = namespace_entity("Lar\Developer");

        $namespace->class('GettersDoc', function ($class) use ($namespace) {
            /** @var ClassEntity $class */
            $this->class = $class;

            $this->create(null, config('gets', []), $class, $namespace);
        });

        file_put_contents('_ide_helper_getter.php', $namespace->wrap('php')->render());
    }

    /**
     * @param  string|int|null  $key
     * @param  string|array  $get
     * @param  ClassEntity  $class
     * @param  NamespaceEntity  $namespace
     * @param  int  $lvl
     * @param  string|null  $groupName
     */
    private function create($key, $get, $class, $namespace, int $lvl = 0, string $groupName = '')
    {
        if (is_array($get)) {
            foreach ($get as $index => $item) {
                $groupName = $groupName.ucfirst(Str::camel($index));

                if (is_array($item)) {
                    $class->doc(function ($doc) use ($index, $groupName) {
                        /** @var DocumentorEntity $doc */
                        $doc->tagProperty('GetterGroup'.$groupName, $index);
                        $doc->tagMethod('GetterGroup'.$groupName, $index, '(...$layers)');
                    });

                    $class2 = $namespace->class('GetterGroup'.$groupName);

                    $this->create($index, $item, $class2, $namespace, $lvl++, $groupName);
                } else {
                    $this->create($index, $item, $class, $namespace, $lvl++, $groupName);
                }
            }
        } else {
            $class->doc(function ($doc) use ($key, $groupName) {
                /** @var DocumentorEntity $doc */
                $doc->tagProperty('GetterGroup'.$groupName, $key);
                $doc->tagMethod('GetterGroup'.$groupName, $key, '(...$layers)');
            });

            $class2 = $namespace->class('GetterGroup'.$groupName);

            $class2->doc(function ($doc) use ($get) {
                /** @var DocumentorEntity $doc */
                $this->create_class($get, $doc);
            });
        }
    }

    /**
     * @param  string  $class
     * @param  DocumentorEntity  $doc
     * @throws ReflectionException
     */
    private function create_class(string $class, $doc)
    {
        if (class_exists($class)) {
            $obj = new $class;

            if ($obj instanceof Getter) {
                $ref = new ReflectionClass($class);

                foreach ($ref->getMethods() as $method) {
                    if ($method->isStatic() && $method->isPublic()) {
                        $var = Str::snake($method->name);

                        $params = trim(ParamEntity::buildFromReflection($method));

                        $ret = DocumentorEntity::parseReturn($method->getDocComment());

                        if (!empty($ret)) {
                            $ret .= '|\\'.$class;
                        } else {
                            $ret = '\\'.$class;
                        }

                        $doc->tagProperty($ret, $var, DocumentorEntity::parseDescription($method->getDocComment()));
                        $doc->tagMethod($ret, $var.'('.(!empty($params) ? "{$params}, " : '').'...$params)',
                            DocumentorEntity::parseDescription($method->getDocComment()));
                    }
                }
            }
        }
    }
}
