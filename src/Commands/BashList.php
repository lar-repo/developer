<?php

namespace Lar\Developer\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;

class BashList extends Command
{
    /**
     * Completion Collection.
     *
     * @var Collection
     */
    public static $completion_collection;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'bash:list';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lists commands for bash completion.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add login in to list.
     *
     * @param  string|array  $executor
     * @return mixed
     */
    public static function addLogic($executor)
    {
        if (is_array($executor)) {
            foreach ($executor as $item) {
                static::addLogic($item);
            }
        }

        if (is_string($executor)) {
            $name = str_replace('_', "\:", Str::snake(last_namespace_element($executor)));

            static::addCommandLogic($name, $executor);
        }
    }

    /**
     * Add logic in to collection.
     *
     * @param  string  $command
     * @param $executor
     */
    public static function addCommandLogic(string $command, string $executor)
    {
        if (!static::$completion_collection) {
            static::$completion_collection = new Collection([]);
        }

        static::$completion_collection->put($command, $executor);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $commandTyped = $this->argument('commandTyped');

        if (static::$completion_collection instanceof Collection) {
            $logic = $this->findLogic($commandTyped);

            if ($logic !== false) {
                echo $logic;
                die;
            }
        }

        $allCommands = array_keys($this->getApplication()->all());

        if ($commandTyped) {
            $colonAt = strpos($commandTyped, ':');
            $possibleCommands = [];
            foreach ($allCommands as $command) {
                // Return command in result if it starts with $commandTyped
                if (strpos($command, $commandTyped) === 0) {
                    // Colons acts as separators in bash, so return only second part if colon is in commandTyped.
                    $possibleCommands[] = $colonAt ? substr($command, $colonAt + 1) : $command;
                }
            }
        } else {
            // Nothing typed, return all
            $possibleCommands = $allCommands;
        }

        $data = implode(' ', $possibleCommands);

        echo $data;
    }

    /**
     * Find additation logic.
     *
     * @param $commandTyped
     * @return string
     */
    private function findLogic($commandTyped)
    {
        foreach (static::$completion_collection as $key => $item) {
            if (preg_match('/.*'.$key.'.*/', $commandTyped)) {
                $object = new $item($this);

                if ($object instanceof Renderable) {
                    return $object->render();
                }
            }
        }

        return false;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['artisan', InputArgument::OPTIONAL, '"artisan"'],
            ['commandTyped', InputArgument::OPTIONAL, 'The (partial) artisan command'],
            ['sector1', InputArgument::OPTIONAL, 'sector 1'],
            ['sector2', InputArgument::OPTIONAL, 'sector 2'],
            ['sector3', InputArgument::OPTIONAL, 'sector 3'],
            ['sector4', InputArgument::OPTIONAL, 'sector 4'],
            ['sector5', InputArgument::OPTIONAL, 'sector 5'],
        ];
    }
}
