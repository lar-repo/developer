<?php

namespace Lar\Developer\Commands;

use App;
use Closure;
use Illuminate\Foundation\Console\RouteListCommand;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RoutesCommand extends RouteListCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'routs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all registered routes';

    /**
     * The router instance.
     *
     * @var Router
     */
    protected $router;

    /**
     * The table headers for the command.
     *
     * @var array
     */
    protected $headers = ['Domain', 'Method', 'URI', 'Name', 'Action', 'Middleware', 'Layout', 'Lang', 'Gets', 'WS'];

    /**
     * The columns to display when using the "compact" flag.
     *
     * @var array
     */
    protected $compactColumns = ['method', 'uri', 'name', 'action', 'middleware', 'layout', 'lang', 'gets', 'ws'];

    /**
     * @var int
     */
    protected $it = 0;

    /**
     * Get the route information for a given route.
     *
     * @param  Route  $route
     * @return array
     */
    protected function getRouteInformation(Route $route)
    {
        $middleware = $this->getMiddleware($route);

        $add = [];

        if ($layout = $this->argument('layout')) {
            if (!preg_match('/layout\:'.$layout.'/', $middleware, $m) || !preg_match('/dom/', $middleware, $m2)) {
                return null;
            } else {
                $add['layout'] = $layout;
            }
        } else {
            if (preg_match('/layout\:([a-zA-Z0-9\,\s\_\.\-]+)*/', $middleware, $m) && preg_match('/dom/', $middleware,
                    $m2)) {
                $middleware = str_replace('||', '|', trim(str_replace($m[0], '', $middleware), '|'));
                $middleware = str_replace('||', '|', trim(str_replace($m2[0], '', $middleware), '|'));
                $add['layout'] = $m[1];
            }
        }

        $middleware = str_replace('layout:', '', $middleware);

        while (preg_match('/gets\:([a-zA-Z0-9\,\s\_\.\-]+)*/', $middleware, $m)) {
            $middleware = str_replace('||', '|', trim($this->str_replace_first($m[0], '', $middleware), '|'));

            $add['gets'] = isset($add['gets']) ? $add['gets'].','.$m[1] : $m[0];
        }

        if (preg_match('/ws_user(\:[a-zA-Z0-9\,\s\_\.\-]+)*/', $middleware, $m) && preg_match('/lserve/', $middleware,
                $m2)) {
            $middleware = str_replace('||', '|', trim(str_replace($m[0], '', $middleware), '|'));
            $middleware = str_replace('||', '|', trim(str_replace($m2[0], '', $middleware), '|'));

            $add['ws'] = '<info>Yes</info>';
        }

        $lang = false;

        if (preg_match('/lang/', $middleware, $m)) {
            $middleware = str_replace('||', '|', trim(str_replace($m[0], '', $middleware), '|'));

            $add['lang'] = '<info>['.strtoupper(App::getLocale()).']</info>';

            $lang = true;
        }

        $methods = implode('|', $route->methods());

        if ($methods == 'GET|HEAD|POST|PUT|PATCH|DELETE|OPTIONS') {
            $methods = 'ANY';
        }

        $uri = $lang ? preg_replace('/^'.App::getLocale().'\/(.*)/', '$1', $route->uri()) : $route->uri();

        $uri = preg_replace('/(\{.*\?\})/', '<info>$1</info>', $uri);
        $uri = preg_replace('/(\{.*[^\?]\})/', '<comment>$1</comment>', $uri);

        return $this->filterRoute(array_merge([
            'domain' => $route->domain(),
            'method' => $methods,
            'uri' => $uri,
            'name' => $route->getName(),
            'action' => ltrim($route->getActionName(), '\\'),
            'middleware' => $middleware,
            'layout' => '<comment>No</comment>',
            'lang' => '<comment>No</comment>',
            'gets' => '<comment>No</comment>',
            'ws' => '<comment>No</comment>',
        ], $add));
    }

    protected function getMiddleware($route)
    {
        return collect($route->gatherMiddleware())->map(function ($middleware) {
            return $middleware instanceof Closure ? 'Closure' : $middleware;
        })->implode('|');
    }

    /**
     * @param $from
     * @param $to
     * @param $content
     * @return string|string[]|null
     */
    protected function str_replace_first($from, $to, $content)
    {
        $from = '/'.preg_quote($from, '/').'/';

        return preg_replace($from, $to, $content, 1);
    }

    /*
     * Get before filters.
     *
     * @param  \Illuminate\Routing\Route  $route
     * @return string
     */

    /**
     * Get the column names to show (lowercase table headers).
     *
     * @return array
     */
    protected function getColumns()
    {
        if ($this->argument('layout')) {
            $key = array_search('Layout', $this->headers);

            if ($key !== false) {
                unset($this->headers[$key]);
            }
        }

        if (!$this->option('action')) {
            $key = array_search('Action', $this->headers);

            if ($key !== false) {
                unset($this->headers[$key]);
            }
        } else {
            $key = array_search('Lang', $this->headers);
            $key2 = array_search('WS', $this->headers);
            $key3 = array_search('Gets', $this->headers);

            if ($key !== false) {
                unset($this->headers[$key]);
            }
            if ($key2 !== false) {
                unset($this->headers[$key2]);
            }
            if ($key3 !== false) {
                unset($this->headers[$key3]);
            }

            if (!$this->argument('layout')) {
                $key = array_search('Name', $this->headers);
                if ($key !== false) {
                    unset($this->headers[$key]);
                }
            }
        }

        $availableColumns = array_map('strtolower', $this->headers);

        if (!$this->option('fool')) {
            return array_intersect($availableColumns, $this->compactColumns);
        }

        if ($columns = $this->option('columns')) {
            return array_intersect($availableColumns, $this->parseColumns($columns));
        }

        return $availableColumns;
    }

    /**
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['layout', InputArgument::OPTIONAL, 'The name of the layout'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            [
                'columns', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                'Columns to include in the route table'
            ],
            ['fool', 'f', InputOption::VALUE_NONE, 'Show all columns'],
            ['json', null, InputOption::VALUE_NONE, 'Output the route list as JSON'],
            ['method', null, InputOption::VALUE_OPTIONAL, 'Filter the routes by method'],
            ['name', null, InputOption::VALUE_OPTIONAL, 'Filter the routes by name'],
            ['path', null, InputOption::VALUE_OPTIONAL, 'Filter the routes by path'],
            ['reverse', 'r', InputOption::VALUE_NONE, 'Reverse the ordering of the routes'],
            ['action', 'a', InputOption::VALUE_NONE, 'Show actions'],
            [
                'sort', null, InputOption::VALUE_OPTIONAL,
                'The column (n, domain, method, uri, name, action, middleware, layout, gets, ws) to sort by', 'uri'
            ],
        ];
    }
}
