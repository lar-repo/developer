<?php

namespace Lar\Developer\Commands;

use Illuminate\Console\Command;
use Reflection;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

class MDGetter extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'lar:md-getter {class} {begin?} {call?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run LAR Console';
    /**
     * @var bool
     */
    protected $begin = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws ReflectionException
     */
    public function handle()
    {
        $obj = $this->argument('class');

        if (!class_exists($obj)) {
            $this->error("Object [{$obj}] not found!");
            die;
        }

        $ref = new ReflectionClass($obj);

        $this->begin = $this->argument('begin');

        $dely = function (ReflectionMethod $method) {
            system('clear');

            $this->comment(str_repeat('#', 30));

            $this->info($this->method($method));

            $this->comment(str_repeat('#', 30));

            $this->confirm('Confirm?', true);
        };

        foreach ($ref->getMethods() as $method) {
            if ($this->begin) {
                if ($method->name == $this->begin) {
                    $this->begin = false;

                    $dely($method);
                }
            } else {
                $dely($method);
            }
        }
    }

    public function method(ReflectionMethod $method)
    {
        $text = $method->getDocComment();

        $desc = pars_description_from_doc($text);

        $text = str_replace($desc, '', $text);

        $text = collect(explode("\n", $text))->map(function ($item) {
            return trim($item);
        })->filter(function ($item) {
            return $item !== '*' && $item !== '/**' && $item !== '*/';
        })->implode("\n ");

        if (!empty($text)) {
            $text = "/**\n ".$text."\n */";
        }

        if (!preg_match('/.*\.$/', $desc) && !empty($desc)) {
            $desc .= '.';
        }

        $MD = '## '.$method->name."\n".'`'.implode(' ',
                Reflection::getModifierNames($method->getModifiers())).'`: '.$desc."\n";

        $MD .= "```php\n";

        if (!empty($text)) {
            $MD .= $text."\n";
        }

        $call = $this->argument('call');

        if (!$call) {
            $call = '$tag->';
        }

        $MD .= $call.$method->name.'('.refl_params_entity($method->getParameters()).');';

        $MD .= "\n```";

        return $MD;
    }
}
