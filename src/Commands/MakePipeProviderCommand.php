<?php

namespace Lar\Developer\Commands;

use Illuminate\Console\Command;
use Lar\Developer\PipelineServiceProvider;

/**
 * Class MakePipeCommand.
 * @package Lar\Developer\Commands
 */
class MakePipeProviderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:pipe_provider';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make pipe provider';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = 'PipeServiceProvider';
        $namespace = 'App\\Providers';
        $path = app_path('/Providers');

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        $class = class_entity($name)->wrap('php');
        $class->namespace($namespace)->extend(PipelineServiceProvider::class);

        $class->prop('protected:pipe_map', []);

        file_put_contents($path.'/'.$name.'.php', $class);

        $this->info("Pipe provider [$namespace\\$name] generated!");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['name', InputArgument::OPTIONAL, 'Name of pipe'],
        ];
    }
}
