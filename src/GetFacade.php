<?php

namespace Lar\Developer;

use Illuminate\Support\Facades\Facade as FacadeIlluminate;

/**
 * Class Facade.
 *
 * @package Lar
 */
class GetFacade extends FacadeIlluminate
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return GetInstance::class;
    }
}
