<?php

namespace Lar\Developer;

use Exception;
use Illuminate\Support\ServiceProvider as ServiceProviderIlluminate;
use Lar\Developer\Commands\DumpAutoload;
use Lar\Developer\Commands\LarGit;
use Lar\Developer\Commands\LarGitCheckout;
use Lar\Developer\Commands\LarGitTag;
use Lar\Developer\Commands\MakeGeneratorCommand;
use Lar\Developer\Commands\MakeGetterCommand;
use Lar\Developer\Commands\MakePipeCommand;
use Lar\Developer\Commands\MakePipeProviderCommand;
use Lar\Developer\Commands\RepositoryMakeCommand;
use Lar\Developer\Commands\RoutesCommand;
use Lar\Developer\Commands\SeedPullCommand;
use Lar\Developer\Commands\SuperVisorCfgGenerator;
use Lar\Developer\Commands\WatcherCommand;
use Lar\Developer\Middleware\GetterMiddleware;

/**
 * Class ServiceProvider.
 *
 * @package Lar\Layout
 */
class ServiceProvider extends ServiceProviderIlluminate
{
    /**
     * @var array
     */
    protected $commands = [
        DumpAutoload::class,
        LarGit::class,
        LarGitCheckout::class,
        LarGitTag::class,
        SuperVisorCfgGenerator::class,
        RoutesCommand::class,
        MakeGetterCommand::class,
        MakePipeCommand::class,
        MakePipeProviderCommand::class,
        WatcherCommand::class,
        MakeGeneratorCommand::class,
        SeedPullCommand::class,
        RepositoryMakeCommand::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'gets' => GetterMiddleware::class,
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     * @throws Exception
     */
    public function boot()
    {
        include __DIR__.'/macroses.php';

        $this->publishes([__DIR__.'/../config' => config_path()], 'lar-dev-config');

        $this->loadTranslationsFrom(__DIR__.'/../langs', 'dev');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * App register provider.
         */
        if (class_exists('App\Providers\PipeServiceProvider')) {
            $this->app->register('App\Providers\PipeServiceProvider');
        }

        $this->mergeConfigFrom(
            __DIR__.'/../config/gets.php', 'gets'
        );

        $this->registerRouteMiddleware();

        $this->commands($this->commands);
    }

    /**
     * Register the route middleware.
     *
     * @return void
     */
    protected function registerRouteMiddleware()
    {
        // register route middleware.
        foreach ($this->routeMiddleware as $key => $middleware) {
            app('router')->aliasMiddleware($key, $middleware);
        }
    }
}
