<?php

namespace Lar\Developer;

use Illuminate\Support\ServiceProvider;
use Lar\Developer\Core\Traits\Piplineble;

/**
 * Class PipelineServiceProvider.
 * @package Lar\Developer
 */
class PipelineServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $pipe_type_map = [];

    /**
     * @var array
     */
    protected $pipe_map = [];

    /**
     * Register.
     */
    public function register()
    {
        parent::register();

        /** @var Piplineble $class */
        foreach ($this->pipe_type_map as $class => $types) {
            foreach ($types as $type => $pipe) {
                $class::pipes($pipe, $type);
            }
        }

        /** @var Piplineble $class */
        foreach ($this->pipe_map as $class => $pipes) {
            $class::pipes($pipes);
        }
    }
}
