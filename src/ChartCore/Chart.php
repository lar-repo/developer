<?php

namespace Lar\Developer\ChartCore;

use Lar\Developer\ChartCore\Interfaces\ChartInterface;
use Lar\Developer\ChartCore\Types\Bar;
use Lar\Developer\ChartCore\Types\Bubble;
use Lar\Developer\ChartCore\Types\Doughnut;
use Lar\Developer\ChartCore\Types\Line;
use Lar\Developer\ChartCore\Types\Pie;
use Lar\Developer\ChartCore\Types\PolarArea;
use Lar\Developer\ChartCore\Types\Radar;
use Lar\Developer\ChartCore\Types\Scatter;

/**
 * Class Chart.
 * @package Lar\Developer\ChartCore
 */
class Chart implements ChartInterface
{
    /**
     * @var ChartInterface
     */
    protected $chart;

    /**
     * @return Bar
     */
    public function bar()
    {
        return new Bar($this);
    }

    /**
     * @return Bubble
     */
    public function bubble()
    {
        return new Bubble($this);
    }

    /**
     * @return Doughnut
     */
    public function doughnut()
    {
        return new Doughnut($this);
    }

    /**
     * @return Line
     */
    public function line()
    {
        return new Line($this);
    }

    /**
     * @return Pie
     */
    public function pie()
    {
        return new Pie($this);
    }

    /**
     * @return PolarArea
     */
    public function polarArea()
    {
        return new PolarArea($this);
    }

    /**
     * @return Radar
     */
    public function radar()
    {
        return new Radar($this);
    }

    /**
     * @return Scatter
     */
    public function scatter()
    {
        return new Scatter($this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->chart->toArray();
    }

    /**
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return $this->chart->toJson();
    }
}
