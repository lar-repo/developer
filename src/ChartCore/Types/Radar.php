<?php

namespace Lar\Developer\ChartCore\Types;

/**
 * Class Radar.
 * @package Lar\Developer\ChartCore\Types
 */
class Radar extends GeneralType
{
    /**
     * @var string
     */
    protected $type = 'radar';
}
