<?php

namespace Lar\Developer\ChartCore\Types;

/**
 * Class Bubble.
 * @package Lar\Developer\ChartCore\Types
 */
class Bubble extends GeneralType
{
    /**
     * @var string
     */
    protected $type = 'bubble';
}
