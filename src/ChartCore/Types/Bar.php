<?php

namespace Lar\Developer\ChartCore\Types;

/**
 * Class Bar.
 * @package Lar\Developer\ChartCore\Types
 */
class Bar extends GeneralType
{
    /**
     * @var string
     */
    protected $type = 'bar';
}
