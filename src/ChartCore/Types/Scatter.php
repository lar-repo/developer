<?php

namespace Lar\Developer\ChartCore\Types;

/**
 * Class Scatter.
 * @package Lar\Developer\ChartCore\Types
 */
class Scatter extends Line
{
    /**
     * @var string
     */
    protected $type = 'scatter';
}
