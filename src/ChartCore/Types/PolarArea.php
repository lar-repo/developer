<?php

namespace Lar\Developer\ChartCore\Types;

/**
 * Class PolarArea.
 * @package Lar\Developer\ChartCore\Types
 */
class PolarArea extends GeneralType
{
    /**
     * @var string
     */
    protected $type = 'polarArea';
}
