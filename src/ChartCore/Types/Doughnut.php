<?php

namespace Lar\Developer\ChartCore\Types;

/**
 * Class Doughnut.
 * @package Lar\Developer\ChartCore\Types
 */
class Doughnut extends Pie
{
    /**
     * @var string
     */
    protected $type = 'doughnut';
}
