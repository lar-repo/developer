<?php

namespace Lar\Developer\ChartCore\Types;

use Lar\Developer\ChartCore\Chart;
use Lar\Developer\ChartCore\Interfaces\ChartInterface;

/**
 * Class GeneralType.
 * @package Lar\Developer\ChartCore\Types
 */
abstract class GeneralType implements ChartInterface
{
    /**
     * @var Chart
     */
    protected $chart;

    /**
     * @var string
     */
    protected $type;

    /**
     * Line constructor.
     * @param  Chart  $chart
     */
    public function __construct(Chart $chart)
    {
        $this->chart = $chart;
    }

    /**
     * @param  int  $options
     * @return false|string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        if (!$this->type) {
            return [];
        }

        return [
            'type' => $this->type,
        ];
    }
}
