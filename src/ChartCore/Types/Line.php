<?php

namespace Lar\Developer\ChartCore\Types;

/**
 * Class Line.
 * @package Lar\Developer\ChartCore\Types
 */
class Line extends GeneralType
{
    /**
     * @var string
     */
    protected $type = 'line';
}
