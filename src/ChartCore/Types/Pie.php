<?php

namespace Lar\Developer\ChartCore\Types;

/**
 * Class Pie.
 * @package Lar\Developer\ChartCore\Types
 */
class Pie extends GeneralType
{
    /**
     * @var string
     */
    protected $type = 'pie';
}
