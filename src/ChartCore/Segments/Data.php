<?php

namespace Lar\Developer\ChartCore\Segments;

use Lar\Developer\ChartCore\Interfaces\ChartInterface;

/**
 * Class Data.
 * @package Lar\Developer\ChartCore\Segments
 */
class Data implements ChartInterface
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param  int  $options
     * @return false|string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }
}
