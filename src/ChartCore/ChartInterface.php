<?php

namespace Lar\Developer\ChartCore\Interfaces;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

/**
 * Interface ChartInterface.
 * @package Lar\Developer\ChartCore\Interfaces
 */
interface ChartInterface extends Arrayable, Jsonable
{
}
