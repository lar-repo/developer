<?php

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\RedirectResponse;
use Illuminate\Pipeline\Pipeline;
use Lar\Developer\ChartCore\Chart;
use Lar\Developer\Core\Select2;
use Lar\Developer\EmbeddedCall;
use Lar\Developer\GetterCollect;
use Lar\Layout\Respond;

if (!function_exists('back_validate')) {
    /**
     * Validator with back response.
     *
     * @param  array  $subject
     * @param  array  $rules
     * @param  array  $messages
     * @return bool|RedirectResponse|Respond
     */
    function back_validate(array $subject, array $rules, array $messages = [])
    {
        $rules_new = [];

        if (request()->has('__only_has')) {
            foreach ($subject as $key => $item) {
                if (isset($rules[$key])) {
                    $rules_new[$key] = $rules[$key];
                }
            }
            $rules = $rules_new;
        }

        if ($result = quick_validate($subject, $rules, $messages)) {
            if (request()->ajax() && !request()->pjax()) {
                foreach ($result->errors()->messages() as $key => $message) {
                    foreach ($message as $item) {
                        Respond::glob()->toast_error($item);
                    }
                }

                if (request()->ajax() && !request()->pjax()) {
                    Respond::glob()->reload();
                }

                return Respond::glob();
            }

            return back()->withInput()->withErrors($result);
        }

        return false;
    }
}

if (!function_exists('quick_validate')) {
    /**
     * Quick validate collection.
     *
     * @param $subject
     * @param  array  $rules
     * @param  array  $messages
     * @return bool|\Illuminate\Contracts\Validation\Validator|\Illuminate\Validation\Validator
     */
    function quick_validate(array $subject, array $rules, array $messages = [])
    {
        $result = \Illuminate\Support\Facades\Validator::make($subject, $rules, $messages);

        if ($result->fails()) {
            return $result;
        }

        return false;
    }
}

if (!function_exists('respond_validate')) {
    /**
     * Quick validate collection.
     *
     * @param $subject
     * @param  array  $rules
     * @param  array  $messages
     * @return \Illuminate\Support\Collection|bool
     */
    function respond_validate($subject, array $rules, array $messages = [])
    {
        return collect($subject)->line_validate($rules, $messages);
    }
}

if (!function_exists('ce_link')) {
    /**
     * Create external link.
     *
     * @param  string  $class
     * @param  array  $params
     * @param  string  $position
     * @return array
     */
    function ce_link(string $class, array $params = [], string $position = 'body')
    {
        return [$class => [array_map('ee_model', $params), $position]];
    }
}

if (!function_exists('ee_model')) {
    /**
     * Encrypt entity model.
     *
     * @param  array  $models
     * @return string|array
     * @throws Exception
     */
    function ee_model(...$models)
    {
        $result = [];

        if (!isset($models[0])) {
            return null;
        }

        if (
            !$models[0] instanceof Relation &&
            !$models[0] instanceof Builder &&
            !$models[0] instanceof Model &&
            !$models[0] instanceof Collection
        ) {
            return count($models) > 1 ? $models : $models[0];
        }

        foreach ($models as $model) {
            $class = null;

            $params = null;

            if ($model instanceof Relation || $model instanceof Builder) {
                $model = $model->get();
            }

            if ($model instanceof Model) {
                if (!$model->getKeyName()) {
                    throw new Exception('The function works only with models that have a primary key.');
                }

                if (!$model->exists) {
                    $model = $model->first();
                }

                $class = get_class($model).'::find';

                $params = $model->{$model->getKeyName()};
            } elseif ($model instanceof Collection) {
                if ($model->count()) {
                    $first = $model->first();

                    if (!$first->getKeyName()) {
                        throw new Exception('The function works only with models that have a primary key.');
                    }

                    $class = get_class($first).'::find';

                    $params = [$model->pluck($first->getKeyName())->toArray()];
                }
            } else {
                throw new Exception('The function must take necessarily any connection with the model.');
            }

            if ($params) {
                $result[$class] = $params;
            }
        }

        return 'model:'.sslEnc(json_encode($result));
    }
}

if (!function_exists('de_model')) {
    /**
     * Decrypt entity model that has been encrypted using the "ee_model" function.
     *
     * @param $data
     * @return mixed
     */
    function de_model($data)
    {
        if (!is_string($data)) {
            return $data;
        }

        if (preg_match('/^model\:(.*)/', $data, $m)) {
            $data = $m[1];

            $data = sslDec($data);

            if (!is_json($data)) {
                return null;
            }

            $data = json_decode($data, 1);

            $data = array_scripts($data);

            $return = count($data) == 1 ? $data[0] : $data;

            return $return;
        } else {
            return $data;
        }
    }
}

if (!function_exists('array_scripts')) {
    /**
     * Array scripting.
     *
     * @param $scripts
     * @return array|mixed|null
     */
    function array_scripts($scripts)
    {
        if (!is_array($scripts)) {
            return de_model($scripts);
        }

        $result = [];

        foreach ($scripts as $class_method => $params) {
            $class = null;

            $method = null;

            $signature = 'public';

            if (strpos($class_method, '::') !== false) {
                $cl = explode('::', $class_method);

                if (class_exists($cl[0])) {
                    $class = $cl[0];
                }

                if (!empty($cl[1])) {
                    $method = $cl[1];
                } else {
                    $signature = 'static';
                }
            } elseif (strpos($class_method, '->') !== false) {
                $cl = explode('->', $class_method);

                if (class_exists($cl[0])) {
                    $class = $cl[0];
                }

                if (!empty($cl[1])) {
                    $method = $cl[1];
                }
            } elseif (class_exists($class_method)) {
                $class = $class_method;
            }

            if ($class) {
                $result[] = array_scripts_call($signature, $class, $method, $params);
            } else {
                $result[$class_method] = de_model($params);
            }
        }

        return $result;
    }

    /**
     * Call Class to the scripting.
     *
     * @param $sign
     * @param $class
     * @param  null  $method
     * @param  array  $params
     * @return mixed
     */
    function array_scripts_call($sign, $class, $method = null, $params = [])
    {
        try {
            if ($sign === 'public') {
                if ($method) {
                    $_obj = new $class();

                    if (is_array($params)) {
                        return $_obj->{$method}(...array_values(array_scripts(array_map('de_model', $params))));
                    } else {
                        return $_obj->{$method}(array_scripts($params));
                    }
                } else {
                    if (is_array($params)) {
                        return new $class(...array_values(array_scripts($params)));
                    } else {
                        return new $class(array_scripts($params));
                    }
                }
            } elseif ($sign === 'static') {
                if ($method) {
                    if (is_array($params)) {
                        //dd(5);

                        return $class::$method(...array_values(array_scripts($params)));
                    } else {
                        //dd(6);

                        return $class::$method(array_scripts($params));
                    }
                } else {
                    throw new Exception('Method not found!');
                }
            }
        } catch (Exception $exception) {
            return $exception;
        }
    }
}

if (!function_exists('butty_date')) {
    function butty_date($time)
    {
        $timestamp = strtotime($time);
        $published = date('d.m.Y', $timestamp);

        if ($published === date('d.m.Y')) {
            return trans('dev::date.today_short', ['time' => date('H:i', $timestamp)]);
        } elseif ($published === date('d.m.Y', strtotime('-1 day'))) {
            return trans('dev::date.yesterday_short', ['time' => date('H:i', $timestamp)]);
        } else {
            $formatted = trans('dev::date.later_short', [
                'date' => date('d F'.(date('Y', $timestamp) === date('Y') ? null : ' Y'), $timestamp),
            ]);

            return strtr($formatted, trans('dev::date.month_declensions'));
        }
    }
}

if (!function_exists('butty_date_time')) {
    function butty_date_time($time)
    {
        $timestamp = strtotime($time);

        $published = date('d.m.Y', $timestamp);

        if ($published === date('d.m.Y')) {
            return trans('dev::date.today', ['time' => date('H:i', $timestamp)]);
        } elseif ($published === date('d.m.Y', strtotime('-1 day'))) {
            return trans('dev::date.yesterday', ['time' => date('H:i', $timestamp)]);
        } else {
            $formatted = trans('dev::date.later', [
                'time' => date('H:i', $timestamp),
                'date' => date('d F'.(date('Y', $timestamp) === date('Y') ? null : ' Y'), $timestamp),
            ]);

            return strtr($formatted, trans('dev::date.month_declensions'));
        }
    }
}

if (!function_exists('get_route_params')) {
    function get_route_params()
    {
        $data = [];

        if ($route = Route::current()) {
            $data = $route->parameters;

            foreach ($data as $key => $item) {
                if ($item instanceof Model) {
                    $data[$key] = $item->getRouteKey();
                }
            }
        }

        return $data;
    }
}

if (!function_exists('on_num_page')) {
    /**
     * Get num page. for pagination.
     *
     * @param  int  $default
     * @param  int  $min
     * @param  int  $max
     * @return int
     */
    function on_num_page(int $default = 5, int $min = 1, int $max = 1000): int
    {
        $num_on_page = (int) request()->get('num', $default);

        if ($num_on_page < $min) {
            $num_on_page = $min;
        }

        if ($num_on_page > $max) {
            $num_on_page = $max;
        }

        return $num_on_page;
    }
}

if (!function_exists('embedded_call') && (int) PHP_VERSION < 8) {
    /**
     * @param $subject
     * @param  array  $arguments
     * @param  null  $throw_event
     * @return mixed
     */
    function embedded_call($subject, array $arguments = [], $throw_event = null)
    {
        return (new EmbeddedCall($subject, $arguments, $throw_event))->call();
    }
}

if (!function_exists('is_embedded_call')) {
    /**
     * @param  mixed  $subject
     * @return bool
     */
    function is_embedded_call($subject)
    {
        return is_string($subject) ? class_exists($subject) : is_callable($subject);
    }
}

if (!function_exists('gets')) {
    /**
     * @return GetterCollect
     */
    function gets()
    {
        return new GetterCollect();
    }
}

if (!function_exists('is_image')) {
    /**
     * Is Image.
     *
     * @param $path
     * @return bool
     */
    function is_image($path)
    {
        try {
            return (bool) exif_imagetype($path);
        } catch (Exception $exception) {
        }

        return false;
    }
}

if (!function_exists('select2')) {
    /**
     * @param  null  $data
     * @param  string|null  $format
     * @param  null  $value
     * @param  string|null  $no_select
     * @return Select2
     * @throws ReflectionException
     */
    function select2($data = null, string $format = null, $value = null, string $no_select = null)
    {
        return new Select2($data, $format, $value, $no_select);
    }
}

if (!function_exists('array_dots_uncollapse')) {
    /**
     * @param  array  $array
     * @param  array  $result
     * @return array
     */
    function array_dots_uncollapse(array $array, array $result = [])
    {
        foreach ($array as $key => $value) {
            Arr::set($result, $key, $value);
        }

        return $result;
    }
}

if (!function_exists('sslEnc')) {
    /**
     * SSL Encoder.
     *
     * @param $data
     * @param  null  $pass
     * @return string
     */
    function sslEnc($data, $pass = null)
    {
        $cipher = config('app.cipher');

        $iv_length = openssl_cipher_iv_length($cipher);

        $key = explode(':', config('app.key'))[1];

        $iv = substr(md5($key), 0, $iv_length);

        if ($pass) {
            $key = $pass;
        }

        if (function_exists('openssl_encrypt')) {
            return urlencode(openssl_encrypt(urlencode($data), $cipher, $key, false, $iv));
        } else {
            return urlencode(exec('echo "'.urlencode($data).'" | openssl enc -'.urlencode($cipher).' -base64 -nosalt -K '.bin2hex($key).' -iv '.bin2hex($key)));
        }
    }
}

if (!function_exists('sslDec')) {
    /**
     * SSL Decoder.
     *
     * @param $data
     * @param  null  $pass
     * @return string
     */
    function sslDec($data, $pass = null)
    {
        $cipher = config('app.cipher');

        $iv_length = openssl_cipher_iv_length($cipher);

        $key = explode(':', config('app.key'))[1];

        $iv = substr(md5($key), 0, $iv_length);

        if ($pass) {
            $key = $pass;
        }

        if (function_exists('openssl_decrypt')) {
            return trim(urldecode(openssl_decrypt(urldecode($data), $cipher, $key, false, $iv)));
        } else {
            return trim(urldecode(exec('echo "'.urldecode($data).'" | openssl enc -'.$cipher.' -d -base64 -nosalt -K '.bin2hex($key).' -iv '.bin2hex($key))));
        }
    }
}

if (!function_exists('ssl_encrypt')) {
    /**
     * Secure SSL Encryption.
     *
     * @param $data
     * @return string
     */
    function ssl_encrypt($data)
    {
        $key = explode(':', config('app.key'))[1];

        $key2 = config('app.key');

        $cipher = config('app.cipher');

        $iv_length = openssl_cipher_iv_length($cipher);

        $iv = openssl_random_pseudo_bytes($iv_length);

        $first_encrypted = openssl_encrypt($data, $cipher, $key, OPENSSL_RAW_DATA, $iv);

        $second_encrypted = hash_hmac('sha3-512', $first_encrypted, $key2, true);

        $output = base64_encode($iv.$second_encrypted.$first_encrypted);

        return $output;
    }
}

if (!function_exists('ssl_decrypt')) {
    /**
     * Secure SSL Decryption.
     *
     * @param $data
     * @return bool|string
     */
    function ssl_decrypt($data)
    {
        $key = explode(':', config('app.key'))[1];

        $key2 = config('app.key');

        $mix = base64_decode($data);

        $cipher = config('app.cipher');

        $iv_length = openssl_cipher_iv_length($cipher);

        $iv = substr($mix, 0, $iv_length);

        $second_encrypted = substr($mix, $iv_length, 64);

        $first_encrypted = substr($mix, $iv_length + 64);

        $data = openssl_decrypt($first_encrypted, $cipher, $key, OPENSSL_RAW_DATA, $iv);

        $second_encrypted_new = hash_hmac('sha3-512', $first_encrypted, $key2, true);

        if (hash_equals($second_encrypted, $second_encrypted_new)) {
            return $data;
        }

        return false;
    }
}

if (!function_exists('is_json')) {
    /**
     * @param $string
     * @param  bool  $return_data
     * @return bool|mixed
     */
    function is_json($string, $return_data = false)
    {
        if (!is_string($string)) {
            return false;
        }

        $data = json_decode($string, 1);

        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : true) : false;
    }
}

if (!function_exists('lang_in_text')) {
    /**
     * @param $string
     * @return bool|mixed
     */
    function lang_in_text($string)
    {
        if (is_string($string)) {
            $string = preg_replace_callback('/\@([a-zA-Z0-9\_\-\.]+)/', function ($m) {
                return Lang::has($m[1]) ? __($m[1]) : '';
            }, $string);
        }

        return $string;
    }
}

if (!function_exists('pipeline')) {
    /**
     * @param $send
     * @param  array  $pipes
     * @return mixed|$send
     */
    function pipeline($send, array $pipes)
    {
        return app(Pipeline::class)
            ->send($send)
            ->through($pipes)
            ->thenReturn();
    }
}

if (!function_exists('chart')) {
    /**
     * @return Chart
     */
    function chart()
    {
        return new Chart();
    }
}

if (!function_exists('tag_replace')) {
    /**
     * @param  string  $text
     * @param  array|object  $materials
     * @param  string  $pattern
     * @return string|string[]|null
     */
    function tag_replace(string $text, $materials, string $pattern = '{*}')
    {
        $pattern = preg_quote($pattern);
        $pattern = str_replace('\*', '([a-zA-Z0-9\_\-\.]+)', $pattern);

        return preg_replace_callback("/{$pattern}/", function ($m) use ($materials) {
            return multi_dot_call($materials, $m[1]);
        }, $text);
    }
}
