<?php

namespace Lar\Developer;

use Arr;
use Exception;
use Get;
use Illuminate\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Log;
use ReflectionClass;
use ReflectionException;
use Str;

/**
 * Class Get.
 * @package Lar\Layout
 */
class GetInstance
{
    /**
     * Simple registered name list.
     *
     * @var array
     */
    public static $list = [];

    /**
     * Access registered name list.
     *
     * @var array
     */
    public static $access_list = [];

    /**
     * @var array
     */
    public static $_tmp_params = [];

    /**
     * @var array
     */
    public static $_tmp_layers = [];

    /**
     * @param  string  $name
     * @return bool
     * @throws Exception
     */
    public function create(string $name = '*')
    {
        if ($name === '*') {
            $gets = config('gets');
            $name = 'gets';
        } else {
            $gets = config("gets.{$name}");
            $name = "gets.{$name}";
        }

        if (isset($gets) && $gets && isset($name) && $name) {
            if (is_array($gets)) {
                foreach (Arr::dot($gets) as $key => $class) {
                    if (is_string($class)) {
                        Get::register($class, $name.'.'.$key);
                    }
                }

                return true;
            } elseif (is_string($gets)) {
                Get::register($gets, $name);

                return true;
            }
        }

        return false;
    }

    /**
     * Register a new getter.
     *
     * @param  string  $class
     * @param  string|null  $name
     * @return bool
     * @throws Exception
     */
    public function register(string $class, string $name = null)
    {
        if (class_exists($class)) {
            $getter = new $class();

            if ($getter instanceof Getter) {
                if ($name) {
                    $getter::$name = $name;
                } else {
                    $getter::$name = 'gets.'.$getter::$name;
                }

                if ($getter::$name && is_string($getter::$name)) {
                    try {
                        $ref = new ReflectionClass($getter);
                    } catch (Exception $exception) {
                        Log::error($exception);

                        return -3;
                    }

                    if (!config($getter::$name)) {
                        config([$getter::$name => $class]);
                    }

                    foreach ($ref->getMethods() as $method) {
                        $name = $getter::$name.'.'.Str::snake($method->name);

                        if ($method->isStatic() && $method->isPublic()) {
                            static::$list[$name] = $class.'::'.$method->name;

                            $var = Str::slug(str_replace('.', '_', $name), '_');

                            static::$access_list[$var] = $name;

                            if (app()->has(static::$access_list[$var])) {
                                return -4;
                            }

                            app()->singleton(static::$access_list[$var],
                                function () use ($getter, $method, $name, $ref, $var) {
                                    $def = $ref->hasMethod('default_'.$method->name) ? $getter->{'default_'.$method->name}() :
                                        ($ref->hasMethod('default') ? $getter->default() : null);

                                    if ($ref->hasMethod('access_'.$method->name)) {
                                        return $getter->{'access_'.$method->name}() ? call_user_func([
                                            $getter, $method->name
                                        ]) : $def;
                                    }

                                    $props = [];

                                    if (isset(static::$_tmp_params[$name])) {
                                        $props = static::$_tmp_params[$name];

                                        unset(static::$_tmp_params[$name]);
                                    }

                                    $layers = [];

                                    if (isset(static::$_tmp_layers[$name])) {
                                        $layers = static::$_tmp_layers[$name];

                                        unset(static::$_tmp_layers[$name]);
                                    }

                                    return $getter->access() ? $this->__layer($layers,
                                        embedded_call([$getter, $method->name], $props), $getter::$name) : $def;
                                });
                        }
                    }

                    //dd(app()->getBindings());

                    return 1;
                } else {
                    return -2;
                }
            } else {
                return -1;
            }
        }

        return 0;
    }

    /**
     * @param  string  $from
     * @param $data
     * @param  string|null  $selector
     * @return mixed
     * @throws ReflectionException
     */
    private function __layer(array $from, $data, string $selector = null)
    {
        foreach ($from as $layer) {
            if (is_array($layer)) {
                $data = $this->__layer($layer, $data);
            } elseif (is_string($layer)) {
                if (app()->has('gets.'.$layer)) {
                    $data = $this->__get_instance('gets.'.$layer, [$data]);
                } elseif (app()->has($selector.'.'.$layer)) {
                    $data = $this->__get_instance($selector.'.'.$layer, [$data]);
                }
            }
        }

        return $data;
    }

    /**
     * @param  string  $name
     * @param  array|null  $arguments
     * @return Application|mixed|null
     * @throws ReflectionException
     */
    protected function __get_instance(string $name, array $arguments = null)
    {
        if (app()->has($name)) {
            if ($arguments !== null) {
                Container::getInstance()->forgetInstance($name);

                self::$_tmp_params[$name] = $arguments;
            }

            return app($name);
        }

        return null;
    }

    /**
     * @return GetterCollect|Application|mixed|null
     */
    public function items()
    {
        return new GetterCollect();
    }

    /**
     * @return array
     */
    public function list()
    {
        return static::$list;
    }

    /**
     * @return array
     */
    public function access_list()
    {
        return static::$access_list;
    }
}
