<?php

namespace Lar\Developer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class CoreRepository.
 *
 * @package Lar\Developer
 *
 * Repository for working with an entity.
 * Can issue datasets, cannot create/modify entities.
 */
abstract class CoreRepository
{
    /**
     * Cache singleton requests.
     * @var array
     */
    protected static $cache = [];
    /**
     * @var Model
     */
    protected $model;
    /**
     * Resource for wrap data.
     * @var string|null
     */
    protected $resource;

    /**
     * CoreRepository constructor.
     */
    public function __construct()
    {
        $class = $this->getModelClass();
        $this->model = is_string($class) ? app($this->getModelClass()) : $class;
    }

    /**
     * Model class namespace getter.
     *
     * @return string|object
     */
    abstract protected function getModelClass();

    /**
     * @param  string  $name
     * @param  array  $arguments
     * @return $this
     */
    public function init_cache(string $name, array $arguments = [])
    {
        $this->re_cache($name, $arguments);

        return $this;
    }

    /**
     * Remove and cache again and get data.
     * @param  string  $name
     * @param  array  $arguments
     * @return mixed
     */
    public function re_cache(string $name, array $arguments = [])
    {
        if ($this->has_cache($name)) {
            unset(static::$cache[static::class][$name]);
        }

        return $this->cache($name, $arguments);
    }

    /**
     * @param  string  $name
     * @return bool
     */
    public function has_cache(string $name)
    {
        return isset(static::$cache[static::class]) && array_key_exists($name, static::$cache[static::class]);
    }

    /**
     * Cache and get method data.
     * @param  string  $name
     * @param  array  $arguments
     * @return mixed
     */
    public function cache(string $name, array $arguments = [])
    {
        if ($this->resource) {
            $resource = $this->resource;

            $this->resource = null;

            return $this->wrap($resource, $name, $arguments);
        } elseif (!$this->has_cache($name)) {
            if (method_exists($this, $name)) {
                static::$cache[static::class][$name] = embedded_call([$this, $name], $arguments);
            } else {
                return null;
            }
        }

        return static::$cache[static::class][$name];
    }

    /**
     * @param  string  $resource
     * @param  string|null  $method
     * @param  array  $arguments
     * @return $this|mixed
     */
    public function wrap(string $resource, string $method = null, array $arguments = [])
    {
        if ($method) {
            $result = $this->cache($method, $arguments);

            if (($result instanceof Collection || $result instanceof LengthAwarePaginator) && method_exists($resource,
                    'collection')) {
                $result = $resource::collection($result);
            } elseif (method_exists($resource, 'make')) {
                $result = $resource::make($result);
            } else {
                $result = new $resource($result);
            }

            return $result;
        }

        return $this->resource($resource);
    }

    public function resource(string $resource)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * @param $equal
     * @param  string  $name
     * @param  array  $arguments
     * @return $this
     */
    public function init_eq_cache($equal, string $name, array $arguments = [])
    {
        if ($equal) {
            $this->re_cache($name, $arguments);
        }

        return $this;
    }

    /**
     * @return Model
     */
    public function model(): Model
    {
        return clone $this->model;
    }

    /**
     * Cache and get.
     * @param  string  $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->cache($name);
    }

    /**
     * @param  string  $name
     * @param $value
     */
    public function __set(string $name, $value)
    {
        static::$cache[static::class][$name] = $value;
    }

    /**
     * @param  string  $name
     * @param  array  $arguments
     * @return mixed
     */
    public function __invoke(string $name, array $arguments = [])
    {
        return $this->re_cache($name, $arguments);
    }
}
