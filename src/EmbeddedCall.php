<?php

namespace Lar\Developer;

use Closure;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;
use Throwable;

/**
 * Class EmbeddedCall.
 * @package Lar\Developer
 */
class EmbeddedCall
{
    /**
     * @var Closure|array
     */
    protected $subject;

    /**
     * @var array
     */
    protected $arguments;

    /**
     * 0 - Closure,
     * 1 - Object.
     *
     * @var string
     */
    protected $mode;

    /**
     * @var ReflectionMethod|ReflectionFunction
     */
    protected $ref;

    /**
     * @var array
     */
    protected $parameters = [];

    /**
     * @var array
     */
    protected $send_parameters = [];

    /**
     * @var array
     */
    protected $route_params = [];

    /**
     * @var Closure
     */
    protected $throw_event;

    /**
     * EmbeddedCall constructor.
     * @param  Closure|array|object|string  $subject
     * @param  array  $arguments
     * @param  Closure|array|null  $throw_event
     * @throws Throwable
     */
    public function __construct($subject, array $arguments = [], $throw_event = null)
    {
        $this->arguments = $arguments;

        $this->throw_event = $throw_event;

        if (request()->route()) {
            $this->route_params = request()->route()->parameters();
        }

        if ($subject instanceof Closure) {
            $this->subject = $subject;

            $this->mode = 0;
        } elseif (is_array($subject) && isset($subject[0]) && isset($subject[1])) {
            $this->subject = [
                is_string($subject[0]) ? new $subject[0] : $subject[0],
                $subject[1],
            ];

            $this->mode = 1;
        } elseif (is_object($subject)) {
            $this->subject = [
                $subject,
                '__invoke',
            ];

            $this->mode = 1;
        } elseif (is_string($subject)) {
            $this->subject = [
                new $subject,
                '__invoke',
            ];

            $this->mode = 1;
        } else {
            $this->throw(new Exception('Invalid subject of call'));
        }

        $this->makeRef();

        $this->makeParameters();
    }

    /**
     * @param  Throwable  $throwable
     * @return $this
     * @throws Throwable
     */
    protected function throw(Throwable $throwable)
    {
        if (is_array($this->throw_event) && isset($this->throw_event[0]) && isset($this->throw_event[1])) {
            return call_user_func($this->throw_event, $throwable);
        } elseif ($this->throw_event instanceof Closure) {
            return ($this->throw_event)($throwable);
        } else {
            throw $throwable;
        }
    }

    /**
     * Make reflection of the call data.
     * @throws Throwable
     */
    protected function makeRef()
    {
        if ($this->mode === 0) {
            try {
                $this->ref = new ReflectionFunction($this->subject);
            } catch (Throwable $throwable) {
                $this->throw($throwable);
            }
        } elseif ($this->mode === 1) {
            try {
                $this->ref = (new ReflectionClass($this->subject[0]))->getMethod($this->subject[1]);
            } catch (Throwable $throwable) {
                $this->throw($throwable);
            }
        } else {
            $this->throw(new Exception('Wrong mode for reflection'));
        }
    }

    /**
     * Make reflection parameters.
     */
    protected function makeParameters()
    {
        foreach ($this->ref->getParameters() as $parameter) {
            list($class, $type, $nullable) = $parameter->hasType() ? (
            !$parameter->getType()->isBuiltin() ?
                [$parameter->getType()->getName(), false, $parameter->getType()->allowsNull()] :
                [false, $parameter->getType()->getName(), $parameter->getType()->allowsNull()]
            ) : [false, false, false];

            $param = [
                'class' => $class,
                'type' => $type,
                'name' => $parameter->getName(),
                'nullable' => $nullable,
                'value' => null,
            ];

            if ($parameter->isDefaultValueAvailable()) {
                $param['default'] = $parameter->getDefaultValue();
            }

            $this->parameters[] = $param;
        }

        $this->toPrepareParameters();
    }

    /**
     * To prepare parameters before call.
     */
    protected function toPrepareParameters()
    {
        foreach ($this->parameters as $key => $parameter) {
            if (isset($this->arguments[$key])) {
                $this->send_parameters[] = $this->arguments[$key];
            } elseif ($parameter['class'] && isset($this->arguments[$parameter['class']])) {
                $this->send_parameters[] = $this->arguments[$parameter['class']];
            } elseif (isset($this->arguments[$parameter['name']])) {
                $this->send_parameters[] = $this->arguments[$parameter['name']];
            } elseif ($parameter['class']) {
                $this->send_parameters[] = $this->makeByClass($parameter, $key);
            } else {
                $this->send_parameters[] = $this->makeByName($parameter, $key);
            }
        }
    }

    /**
     * @param  array  $params
     * @param  int  $key
     * @return string
     */
    protected function makeByClass(array $params, int $key)
    {
        if (app()->has($params['class'])) {
            $class = app($params['class']);

            if ($class instanceof Generator) {
                $this->setGeneratorProps($class);
            }

            return $class;
        } elseif (class_exists($params['class'])) {
            $testClass = new ReflectionClass($params['class']);

            if ($testClass->isAbstract()) {
                return null;
            }

            if (!isset($r_data) && request()->hasFile($params['name'])) {
                $r_data = request()->file($params['name']);
            } elseif (request()->has($params['name'])) {
                $r_data = request()->get($params['name']);
            } elseif (isset($this->route_params[$params['name']])) {
                $r_data = $this->route_params[$params['name']];
            }

            $class = isset($r_data) && is_object($r_data) ? $r_data : new $params['class'];

            if ($class instanceof Generator) {
                $this->setGeneratorProps($class);
            }

            $this->parameters[$key]['class'] = $class;

            if ($class instanceof Model && isset($r_data) && is_numeric($r_data)) {
                $find_class = $class->find($r_data);
                if ($params['nullable']) {
                    $class = $find_class;
                } elseif ($find_class) {
                    $class = $find_class;
                }
            } elseif ($class instanceof FormRequest) {
                app()->bind($params['class']);

                $class = app($params['class']);
            }

            return $class;
        }

        return null;
    }

    /**
     * @param  Generator  $class
     */
    protected function setGeneratorProps(Generator $class)
    {
        foreach (get_object_vars($class) as $key => $get_object_var) {
            if ($key == 'ARGS') {
                $class->{$key} = $this->arguments;
            } elseif (is_string($get_object_var) && isset($this->arguments[$get_object_var])) {
                $class->{$key} = $this->arguments[$get_object_var];
            }
        }
    }

    /**
     * @param  array  $params
     * @param  int  $key
     * @return string
     */
    protected function makeByName(array $params, int $key)
    {
        if (app()->has($params['name'])) {
            return app($params['name']);
        } elseif (isset($this->route_params[$params['name']])) {
            return $this->route_params[$params['name']];
        } elseif (request()->has($params['name'])) {
            return request($params['name']);
        } elseif (isset($params['default'])) {
            return $params['default'];
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function call()
    {
        try {
            $result = call_user_func_array($this->subject, $this->send_parameters);
        } catch (Throwable $throwable) {
            return $this->throw($throwable);
        }

        return $result;
    }
}
