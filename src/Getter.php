<?php

namespace Lar\Developer;

/**
 * Class Getter.
 * @package Lar\Developer
 */
abstract class Getter
{
    /**
     * Register name
     * (In dot style "my.name").
     *
     * @var bool|string
     */
    public static $name = false;

    /**
     * @return bool
     */
    public function access()
    {
        return true;
    }
}
