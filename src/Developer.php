<?php

namespace Lar\Developer;

class Developer
{
    /**
     * @var array
     */
    public static $_dump_model = [];

    /**
     * @param $ee_model
     * @return bool|mixed
     */
    public static function _dump_model($ee_model)
    {
        $k = md5($ee_model);

        if (isset(static::$_dump_model[$k])) {
            return static::$_dump_model[$k];
        }

        return false;
    }

    /**
     * @param $ee_model
     * @param $model
     */
    public static function _dump_add($ee_model, $model)
    {
        $k = md5($ee_model);

        static::$_dump_model[$k] = $model;
    }
}
