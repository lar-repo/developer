<?php

namespace Lar\Developer;

use Illuminate\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Lar\Developer\Core\Traits\Eventable;
use ReflectionException;

/**
 * Class Getter.
 * @package Lar\Developer
 * @mixin GettersDoc
 */
class GetterCollect
{
    use Eventable;

    /**
     * Current selector.
     *
     * @var string|null
     */
    private $selector = 'gets';

    /**
     * @var array
     */
    private $layers = [];

    /**
     * @var array
     */
    private $wrappers = [];

    /**
     * Magic getter.
     *
     * @param $name
     * @return Application|mixed|null
     * @throws ReflectionException
     */
    public function __get($name)
    {
        $test_name = $this->selector.'.'.$name;

        $test_config = config($test_name, false);

        if ($test_config !== false) {
            $this->selector .= ".{$name}";

            return $this;
        } elseif (app()->has($test_name)) {
            $event = ltrim($test_name, 'gets.').'.get_instance';

            $this->callEvent($event);

            GetInstance::$_tmp_layers[$test_name] = $this->layers;

            $selector = $this->selector;

            return $this->__wrap($this->__get_instance($test_name, (count($this->layers) ? [] : null)), $selector);
        }

        return null;
    }

    /**
     * @param $data
     * @param  string|null  $selector
     * @return mixed
     * @throws ReflectionException
     */
    private function __wrap($data, string $selector = null)
    {
        foreach ($this->wrappers as $wrapper) {
            if (is_string($wrapper)) {
                if (app()->has($wrapper)) {
                    $data = $this->__get_instance($wrapper, [$data]);
                } elseif (app()->has('gets.'.$wrapper)) {
                    $data = $this->__get_instance('gets.'.$wrapper, [$data]);
                } elseif (app()->has($selector.'.'.$wrapper)) {
                    $data = $this->__get_instance($selector.'.'.$wrapper, [$data]);
                } elseif (function_exists($wrapper)) {
                    $data = $wrapper($data);
                } elseif (class_exists($wrapper)) {
                    $_wrap = new $wrapper;

                    $data = $_wrap->handle($data);
                }
            } elseif (is_embedded_call($wrapper)) {
                $data = call_user_func($wrapper, $data);
            }
        }

        return $data;
    }

    /**
     * @param  string  $name
     * @param  array|null  $arguments
     * @return Application|mixed|null
     * @throws ReflectionException
     */
    protected function __get_instance(string $name, array $arguments = null)
    {
        $event = ltrim($this->selector, 'gets.').'.instance';

        if (app()->has($name)) {
            if ($arguments !== null) {
                Container::getInstance()->forgetInstance($name);

                GetInstance::$_tmp_params[$name] = $arguments;
            }

            $this->callEvent($event);

            $this->selector = 'gets';

            return app($name);
        }

        return null;
    }

    /**
     * Magic call.
     *
     * @param $name
     * @param $arguments
     * @return Application|mixed|null
     * @throws ReflectionException
     */
    public function __call($name, $arguments)
    {
        $test_name = $this->selector.'.'.$name;

        $test_config = config($test_name, false);

        if ($test_config !== false) {
            $this->selector .= ".{$name}";

            $this->layers = array_merge($this->layers, $arguments);

            return $this;
        } elseif (app()->has($test_name)) {
            $event = ltrim($test_name, 'gets.').'.create_instance';

            $this->callEvent($event);

            GetInstance::$_tmp_layers[$test_name] = $this->layers;

            $selector = $this->selector;

            return $this->__wrap($this->__get_instance($test_name, $arguments), $selector);
        }

        return null;
    }

    /**
     * @param  mixed  ...$wrappers
     * @return $this
     */
    public function wrap(...$wrappers)
    {
        $this->wrappers = array_merge($this->wrappers, $wrappers);

        return $this;
    }

    /**
     * Magic info.
     *
     * @return array
     * @throws ReflectionException
     */
    public function __debugInfo()
    {
        $return = config($this->selector);

        if (is_string($return) && class_exists($return)) {
            $return = ['getter' => $return];
        }

        $return['selector'] = $this->selector;
        $return['layers'] = $this->layers;

        return $return;
    }
}
