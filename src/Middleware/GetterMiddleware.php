<?php

namespace Lar\Developer\Middleware;

use Closure;
use Exception;
use Get;
use Illuminate\Http\Request;

/**
 * Class GetterMiddleware.
 *
 * @package Lar\Developer\Middleware
 */
class GetterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @param  mixed  ...$getters
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next, ...$getters)
    {
        foreach ($getters as $getter) {
            Get::create($getter);
        }

        return $next($request);
    }
}
