<?php

return [
    'later' => ':date at :time',
    'today' => 'today at :time',
    'yesterday' => 'yesterday at :time',
    'later_short' => ':date',
    'today_short' => 'today',
    'yesterday_short' => 'yesterday',
    'month_declensions' => [
        'January' => 'January',
        'February' => 'February',
        'March' => 'March',
        'April' => 'April',
        'May' => 'May',
        'June' => 'June',
        'July' => 'July',
        'August' => 'August',
        'September' => 'September',
        'October' => 'October',
        'November' => 'November',
        'December' => 'December',
    ],
];
