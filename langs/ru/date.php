<?php

return [
    'later' => ':date в :time',
    'today' => 'сегодня в :time',
    'yesterday' => 'вчера в :time',
    'later_short' => ':date',
    'today_short' => 'сегодня',
    'yesterday_short' => 'вчера',
    'month_declensions' => [
        'January' => 'Января',
        'February' => 'Февраля',
        'March' => 'Марта',
        'April' => 'Апреля',
        'May' => 'Мая',
        'June' => 'Июня',
        'July' => 'Июля',
        'August' => 'Августа',
        'September' => 'Сентября',
        'October' => 'Октября',
        'November' => 'Ноября',
        'December' => 'Декабря',
    ],
];
