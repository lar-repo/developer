const timers = {};
const timers_play = {};

module.exports = {

    runCommand: (command, work_dir, play, event) => {

        if (command in timers) {
            clearTimeout(timers[command]);
        }

        timers[command] = setTimeout((cmd) => {
            const {exec} = require("child_process");
            console.log(`[ '${cmd}' ]`);
            exec(`cd  ${work_dir} && ${cmd.replace('{event}', event)}`, (error, stdout, stderr) => {
                if (error) {
                    console.error(error.message);
                    return;
                }
                if (stderr) {
                    console.error(stderr);
                    return;
                }
                if (stdout.trim()) {
                    //console.log(stdout);
                }
            }).on('exit', () => {
                if (command in timers_play) {
                    clearTimeout(timers_play[command]);
                }
                timers_play[command] = setTimeout(() => {
                    play();
                }, 200);
            });
            clearTimeout(timers[cmd]);
        }, 500, command);
    }
};
