const fs = require('fs');

if (2 in process.argv && 3 in process.argv) {

    let work_dir = process.argv[3];

    let data = Buffer.from(process.argv[2], 'base64').toString();

    try {

        data = JSON.parse(data);

    } catch (e) {
        process.exit(500);
    }

    require('./run_events')(data, work_dir);
}
