const chokidar = require('chokidar');
const helpers = require('./helpers');

module.exports = (data, work_dir) => {

    Object.keys(data).map((path) => {

        const timers = {};
        const sizes = {};
        let stop = false;

        let commands = data[path];

        let watcher = chokidar.watch(path, {
            ignored: /(^|[\/\\])\../,
            persistent: true
        });

        let play = () => {
            stop = false;
        }

        let log = (p, e, s) => {

            console.log([path], [e], ['Commands count:', commands.length]);
            console.log([p]);

            if (Array.isArray(commands)) {

                commands.map((command) => {

                    helpers.runCommand(command, work_dir, play, e);
                });

            } else if (typeof commands === 'string') {

                helpers.runCommand(commands, work_dir, play, e);
            }
        };

        ['add', 'change', 'unlink', 'addDir', 'unlinkDir'].map((e) => {

            watcher
                .on(e, (p, s) => {
                    if (!s && !stop) {
                        if (path in timers) {
                            clearTimeout(timers[path]);
                        }
                        timers[path] = setTimeout(() => {
                            log(p, e, s);
                        }, 500);
                        stop = true;
                    }
                });
        });
    });
};
