# Getter mechanism

## About

Всем понятно, для того что бы в шаблоне (или где либо еще) использовать какие либо данные, вам необходимо их подготовить
и передать или обернуть в какую-то функцию и вызывать её по надобности. Данные подходы как правило не структурированы и
зачастую бывают неудобными, да есть сторонние пакеты типа
`L5_repository` но все эти механизмы хоть и структурированы но очень грамосткие. Для того что бы устранить эти
неудобства я разработал Getter подход.

Покажу вам пример как получать данные с помощью `Getter`:

```php
/**
 * @return \Illuminate\Database\Eloquent\Collection|\App\Models\User[]
 */
gets()->users->active;
```

Данное свойство условно предназначено для получения списка активных пользователей из базы данных. И при следуюших
вызовах этого свойства повторное обращение в базу данных не будет происходить, вы будите всегда получать один и тот-же
результат, так как это по факту является паттерном `Singleton`. Бываю конешно моменты когда необходимо заново выполнить
свойство и обновить данные, для этого вызывает это свойство как метод:

```php
/**
 * @return \Illuminate\Database\Eloquent\Collection|\App\Models\User[]
 */
gets()->users->active();
```

## Object

Итак, пришло время создать первый `Getter`, да для этого подготовлена специальная
`artisan` команда, но об этом чуть позже, сейчас расмотрим самостоятельное создание объекта.

Для начала создадим папку для объектов `app/Models/Getters`. Я привык модели хранить не так как принято в `Laravel` а в
отдельной директонии `Models`. И там-же я храню все геттеры.

Далее создаем сам объект:

```php
<?php

namespace App\Models\Getters;

use Lar\Developer\Getter;

/**
 * Users Class
 * 
 * @package App\Models\Getters
 */
class Users extends Getter
{
    /**
     * Public method access
     * 
     * @return bool
     */
    public function access() {
        
        return true;
    }
    
    /**
     * Public method default
     * 
     * @return mixed
     */
    public function default() {
        
        return null;
    }
}
``` 

## Methods

Теперь о стандартых методах:

1. `access` - Метод для определения доступности ко всем методам геттера. На пример это может быть проверка на роль или
   на гостя и так далее.
2. `access_*` - Индивидуальный метод для определения доступности.
3. `default` - Метод который возвращает стандартные данные если к геттеру закрыт доступ методом `access` или `access_*`.
4. `default_*` - Индивидуальный метод который возвращает стандартные данные если к геттеру закрыт доступ
   методом `access` или `access_*`.

Если у геттера есть индивидуальные методы то глобальные к ниму не применяются.

Все создаваемые геттеры объекта должны быть статические и естественоо публичны.

Пример геттера для поучения активных пользователей:

```php
/**
 * @return \App\Models\User[]|\Illuminate\Database\Eloquent\Collection
 */
public static function active () {

    return \App\Models\User::whereActive(1)->get();
}
``` 

но вызвать его у вас еще не выйдет.

## Object registration

У вас есть 2 пути развития событий:

1. Зарегистрировать его глобально через алиас `Get`
2. Вписать класс в конфиги проэкта (основной подход)

### 1. Global registration

Для того что бы глобально зарегестрировать геттер вам необходимо добавить запись в ваш `ServiceProvider`:

```php
/**
 * Bootstrap any application services.
 *
 * @return void
 * @throws \Exception
 */
public function boot()
{
    \Get::register(\App\Models\Getters\Users::class, 'users');
}
```

Рассмотрим метод `register`:

```php
/**
 * Register a new getter
 *
 * @param string $class
 * @param string|null $name
 * @return bool
 * @throws \Exception
 */
public function register(string $class, string $name = null)
```

Второй параметр `name` не обязателен только в том случае если у вас в геттере прописано статическое свойство `name`.

Тереь у вас будет доступен этот геттер (Блоее детально о вызовах - далее)

### 2. Config registration

Основной метод (рекомкндуемый). Данная регистрация геттеров примущественно тем что вы можете группировать свои геттеры и
подключать их групами или по одиночке к роуту с помощю `Middleware` или создать глобально.

Для начала необходимо прописать в конфигах свой геттер к примеру:

```php
<?php

return [
    'users' => 'App\\Models\\Getters\\Users',
     // or group
     'system' => [
        'users' => 'App\\Models\\Getters\\Users',
        ...
     ]
];
```

и подключить его к роуту или создать глобальный доступ.

#### Route create

Для подключения к роуту необходимо добавить `Middleware` к необходимоту роуту или группе роутов.

##### Подключение одного геттера:

```php
Route::middleware('gets:users')->get('/home', 'HomeController@index');
```

##### Подключение группы геттеров

(любая группа - это вложенность массива в `config/gets.php`):

```php
Route::middleware('gets:system')->get('/home', 'HomeController@index');
```

`system` - это группа которая может включать в себя сколько угодно потгруп и геттеров.

##### Подключение нескольких геттеров:

```php
Route::middleware('gets:user,system')->get('/home', 'HomeController@index');
```

данный пример подключит как геррер `users` так и группу `system`

##### Пример подключения геттера из группы:

```php
Route::middleware('gets:system.users')->get('/home', 'HomeController@index');
```

#### Global create

Для того что бы глобально создать геттер вам необходимо добавить запись в ваш `ServiceProvider`:

```php
/**
 * Bootstrap any application services.
 *
 * @return void
 * @throws \Exception
 */
public function boot()
{
    \Get::cretae('users');
    //or group
    \Get::cretae('system');
    //or group method
    \Get::cretae('system.users');
}
```

Рассмотрим метод `cretae`:

```php
/**
 * @param  string  $name
 * @throws \Exception
 * @return bool
 */
public function create(string $name)
```
